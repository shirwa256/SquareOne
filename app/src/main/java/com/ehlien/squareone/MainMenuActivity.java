package com.ehlien.squareone;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.*;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.internal.GamesClientImpl;
import com.google.android.gms.games.leaderboard.ScoreSubmissionData;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.google.example.games.basegameutils.BaseGameUtils;


public class MainMenuActivity extends BaseGameActivity implements View.OnClickListener {

    Button play,leaderboard,share;
    TextView title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);


        final Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(),"FuturaLT/Helvetica-Condensed-Light-Light.ttf");

        play = (Button)findViewById(R.id.playButton);
        leaderboard = (Button)findViewById(R.id.lbButton);

        share = (Button) findViewById(R.id.shareButton);

        title = (TextView)findViewById(R.id.titleName);


        play.setTypeface(font);
        leaderboard.setTypeface(font);

        share.setTypeface(font);

        title.setTypeface(font);

        play.setTextSize(20.0f);
        leaderboard.setTextSize(20.0f);

        share.setTextSize(20.0f);




        play.setOnClickListener(this);
        leaderboard.setOnClickListener(this);
        share.setOnClickListener(this);

        if(isNetworkOnline()){
            beginUserInitiatedSignIn();
        }else{
            getGameHelper().setMaxAutoSignInAttempts(0);
        }


        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

    }

    public boolean isNetworkOnline() {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;

    }

    @Override
    public void onClick(View v) {
        MediaPlayer mp = MediaPlayer.create(this, R.raw.tap);
        mp.start();
        switch (v.getId()) {
            case R.id.playButton:

                Intent i = new Intent(this, GameActivity.class);
                startActivity(i);
                break;
            case R.id.lbButton:
                if (v.getId() == R.id.lbButton && getApiClient().isConnected()) {
                    startActivityForResult(Games.Leaderboards.getLeaderboardIntent(
                            getApiClient(), getString(R.string.leaderboard_finding_the_chosen_one)), 100);
                } else {
                    Toast.makeText(MainMenuActivity.this, "Please Sign In", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.shareButton:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Found this awesome game call #SquareOne for iOS & android RT to share the love!");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

        }
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }
}
