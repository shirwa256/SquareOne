package com.ehlien.squareone;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LaunchActivity extends AppCompatActivity {
    private final int DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                    Intent showHomeActivity = new Intent(getApplicationContext(), MainMenuActivity.class);
                    startActivity(showHomeActivity);

                finish();
            }
        }, DISPLAY_LENGTH);
    }
}
