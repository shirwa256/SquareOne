package com.ehlien.squareone;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.leaderboard.LeaderboardScore;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.example.games.basegameutils.BaseGameActivity;

import java.text.DecimalFormat;

public class GameOverActivity extends BaseGameActivity implements View.OnClickListener {

    private Button share,play,home;
    private TextView gameoverTitle,gmScore,hsScore,scoreTitle,hsScoreTitle;
    private InterstitialAd interstitialAd;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        interstitialAd = new InterstitialAd(GameOverActivity.this);
        interstitialAd.setAdUnitId(getString(R.string.bigAd));
        AdView adView = (AdView) this.findViewById(R.id.adView);

        // Request for Ads
        AdRequest adRequest = new AdRequest.Builder()

                // Add a test device to show Test Ads
                .build();

        // Load ads into Banner Ads
        adView.loadAd(adRequest);

        // Load ads into Interstitial Ads
        interstitialAd.loadAd(adRequest);

        // Prepare an Interstitial Ad Listener
        interstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
               // displayInterstitial();
            }
                @Override
                public void onAdClosed() {

                    //Begin Game, continue with app

                }
            });

        final Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(),"FuturaLT/Helvetica-Condensed-Light-Light.ttf");

        share = (Button)findViewById(R.id.gameOverShare);
        play = (Button)findViewById(R.id.gameOverPlay);
        home = (Button)findViewById(R.id.gameOverHome);
        gameoverTitle = (TextView)findViewById(R.id.gameOverTitle);
        gmScore = (TextView)findViewById(R.id.scorePoint);
        hsScore = (TextView)findViewById(R.id.highScorePoint);
        scoreTitle = (TextView)findViewById(R.id.scoreLabel);
        hsScoreTitle = (TextView)findViewById(R.id.highScoreLabel);


        gameoverTitle.setTypeface(font);
        gmScore.setTypeface(font);
        hsScore.setTypeface(font);
        scoreTitle.setTypeface(font);
        hsScoreTitle.setTypeface(font);
        share.setTypeface(font);
        play.setTypeface(font);
        home.setTypeface(font);

        gameoverTitle.setTextSize(60.0f);
        gmScore.setTextSize(25.0f);
        hsScore.setTextSize(25.0f);
        scoreTitle.setTextSize(25.0f);
        hsScoreTitle.setTextSize(25.0f);
        play.setTextSize(20.0f);
        share.setTextSize(20.0f);
        home.setTextSize(20.0f);


        share.setOnClickListener(this);
        play.setOnClickListener(this);
        home.setOnClickListener(this);

        if(isNetworkOnline()){
            beginUserInitiatedSignIn();
        }else{
            getGameHelper().setMaxAutoSignInAttempts(0);
        }

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        String rScore = (String)b.get("score");
        String hScore = (String)b.get("highScore");

           gmScore.setText(rScore);

        if(Integer.parseInt(rScore) != 000) {
            if (Integer.parseInt(rScore) < Integer.parseInt(hScore)) {
                hsScore.setText(hScore);
            } else if(hScore == null || Integer.parseInt(rScore) > Integer.parseInt(hScore)) {
                hsScore.setText(rScore);
            }
        }else{
            hsScore.setText(hScore);
        }




    }

    public void displayInterstitial() {
        // If Ads are loaded, show Interstitial else show nothing.
        if (interstitialAd.isLoaded()) {
            interstitialAd.show();
        }
    }

    public boolean isNetworkOnline() {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;

    }


    @Override
    public void onClick(View view) {
        MediaPlayer mp = MediaPlayer.create(this, R.raw.pop);
        mp.start();
        switch(view.getId()){
            case R.id.gameOverShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Try beat my high score #SquareOne for iOS & " +
                                                        "Android, It's "+hsScore.getText());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.gameOverPlay:
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else{
                    //Begin Game, continue with app
                    Intent game = new Intent(GameOverActivity.this, GameActivity.class);
                    startActivity(game);
                    finish();
                }
                break;
            case R.id.gameOverHome:
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }
                    Intent main = new Intent(GameOverActivity.this, MainMenuActivity.class);
                    startActivity(main);
                    finish();

                break;
        }



    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }


    public static String formatScore(long score) {
        DecimalFormat nf3 = new DecimalFormat("000");


        return     nf3.format(score);

    }
}
