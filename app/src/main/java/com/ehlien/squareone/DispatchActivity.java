package com.ehlien.squareone;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by DevStation on 2016-05-08.
 */
public class DispatchActivity extends AppCompatActivity {

    private final int DISPLAY_LENGTH = 10000;
    private TextView Loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch);
        final Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(),"FuturaLT/FuturaLT-CondensedLight.ttf");

        Loading = (TextView)findViewById(R.id.text1);


        Loading.setTypeface(font);

        Loading.setTextSize(25.0f);


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                Intent showHomeActivity = new Intent(getApplicationContext(), GameActivity.class);
                startActivity(showHomeActivity);

                finish();
            }
        }, DISPLAY_LENGTH);
    }

}
