package com.ehlien.squareone;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.example.games.basegameutils.BaseGameActivity;
import com.google.example.games.basegameutils.BaseGameUtils;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends BaseGameActivity {

    private ImageView aTR,aTL,aBR,aBL;
    private ImageView bTR,bTL,bBR,bBL;
    private ImageView cTR,cTL,cBR,cBL;
    private ImageView dTR,dTL,dBR,dBL;
    private ImageView [] q = new ImageView[4];

    public int [] qArray,aArray,bArray,cArray,dArray;
    public float[] rotateArray = {90.0f,180.0f,270.0f,360.0f};
    public int colorNum[] = {Color.parseColor("#4ECDC4"),Color.parseColor("#C7F464"),
            Color.parseColor("#C44D58"), Color.parseColor("#FF6B6B")};
    private LinearLayout qLayout,aLayout,bLayout,cLayout,dLayout;
    private int[][] answerArray;
    private TextView timer,score;
    private long scoreCounter = 000;
    private int countdown = 5;
    private int scoreAdder=0;
    private long mPoints =0;
    static AudioTrack gameMusic;

    Intent gameOverIntent;
    private MyCountDown t = new MyCountDown(5800, 1000);
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);


        gameOverIntent = new Intent(this,GameOverActivity.class);







        answerArray = new int[4][4];
        final Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(),"FuturaLT/FuturaLT-CondensedLight.ttf");

        timer = (TextView)findViewById(R.id.timer);
        score = (TextView)findViewById(R.id.score);

        timer.setTypeface(font);
        score.setTypeface(font);
        timer.setTextSize(30.0f);
        score.setTextSize(30.0f);

        //setting the images for the squares
        q[0] = (ImageView) findViewById(R.id.imageTR);
        q[1] = (ImageView) findViewById(R.id.imageTL);
        q[2] = (ImageView) findViewById(R.id.imageBR);
        q[3] = (ImageView) findViewById(R.id.imageBL);


        aTR = (ImageView) findViewById(R.id.aTR);
        aTL = (ImageView) findViewById(R.id.aTL);
        aBL = (ImageView) findViewById(R.id.aBL);
        aBR = (ImageView) findViewById(R.id.aBR);

        bTR = (ImageView) findViewById(R.id.bTR);
        bTL = (ImageView) findViewById(R.id.bTL);
        bBL = (ImageView) findViewById(R.id.bBL);
        bBR = (ImageView) findViewById(R.id.bBR);

        cTR = (ImageView) findViewById(R.id.cTR);
        cTL = (ImageView) findViewById(R.id.cTL);
        cBL = (ImageView) findViewById(R.id.cBL);
        cBR = (ImageView) findViewById(R.id.cBR);

        dTR = (ImageView) findViewById(R.id.dTR);
        dTL = (ImageView) findViewById(R.id.dTL);
        dBL = (ImageView) findViewById(R.id.dBL);
        dBR = (ImageView) findViewById(R.id.dBR);

        //setting the LinearLayout button
        qLayout = (LinearLayout) findViewById(R.id.qCLayout);
        aLayout = (LinearLayout) findViewById(R.id.aCLayout);




        aLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int  [] answer = answerArray[0];
                aButton(view,answer);
            }
        });



        bLayout = (LinearLayout) findViewById(R.id.bCLayout);
        bLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int  [] answer = answerArray[1];
                aButton(view,answer);
            }
        });


        cLayout = (LinearLayout) findViewById(R.id.cCLayout);

        cLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int  [] answer = answerArray[2];
                aButton(view,answer);
            }
        });


        dLayout = (LinearLayout) findViewById(R.id.dCLayout);

        dLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int  [] answer = answerArray[3];
                aButton(view,answer);
            }
        });

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        GameState();
        if(!isNetworkOnline()) {
            getGameHelper().setMaxAutoSignInAttempts(0);
        }
        loadScoreOfLeaderBoard();



    }

    public boolean isNetworkOnline() {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            t.cancel();
            finish();
        }
        else if((keyCode == KeyEvent.KEYCODE_HOME)){
            t.cancel();
            finish();
        }else if((keyCode == KeyEvent.KEYCODE_APP_SWITCH)){
            t.cancel();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void aButton(View view,int [] answer){

        if (Arrays.equals(answer,qArray)) {
            MediaPlayer mp = MediaPlayer.create(GameActivity.this, R.raw.right);
            mp.start();
            GameState();
            t.start();
            scoreCounter+=scoreAdder+1;
            score.setText(String.valueOf(formatScore(scoreCounter)));
        }else{
            MediaPlayer mp = MediaPlayer.create(GameActivity.this, R.raw.wrong);
            mp.start();
            if(isSignedIn()) {
                Games.Leaderboards.submitScore(getApiClient(),
                        getString(R.string.leaderboard_finding_the_chosen_one), scoreCounter);
            }
            String s = String.valueOf(mPoints);
            gameOverIntent.putExtra("highScore",s);
            gameOverIntent.putExtra("score",score.getText());
            startActivity(gameOverIntent);
            t.cancel();
            GameActivity.this.finish();
        }

    }





    /*
    class myLeaderBoardSubmitScoreCallback implements ResultCallback<Leaderboards.SubmitScoreResult> {
        @Override
        public void onResult(Leaderboards.SubmitScoreResult res) {
            if (res.getStatus().getStatusCode() == 0) {
                // data sent successfully to server.
                // display toast.
                Toast.makeText(GameActivity.this, "leader", Toast.LENGTH_SHORT).show();

            }else{
                Toast.makeText(GameActivity.this, "not leader", Toast.LENGTH_SHORT).show();
            }
        }
    }*/


    public int randomInt(int index){

        Random rndNxt = new Random();

        return rndNxt.nextInt(index);
    }

    public int [] randomIntArray(int index){

        Random rndNxt = new Random();

        int [] rndIntArray = {rndNxt.nextInt(index),rndNxt.nextInt(index),
                rndNxt.nextInt(index),rndNxt.nextInt(index)};

        return rndIntArray;
    }

    static void shuffleArray(int[][] ar)
    {

        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--)
        {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a[] = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }


    public void GameState() {

        int [] main = randomIntArray(3);
        qArray = main.clone();
        aArray = main.clone();
        bArray = randomIntArray(3);
        cArray = randomIntArray(3);
        dArray = randomIntArray(3);


        answerArray[0] = aArray;
        answerArray[1] = bArray;
        answerArray[2] = cArray;
        answerArray[3] = dArray;


        shuffleArray(answerArray);

        qLayout.setRotation(rotateArray[randomInt(rotateArray.length)]);
        q[0].setBackgroundColor(colorNum[qArray[0]]);
        q[1].setBackgroundColor(colorNum[qArray[1]]);
        q[2].setBackgroundColor(colorNum[qArray[2]]);
        q[3].setBackgroundColor(colorNum[qArray[3]]);

        aTR.setBackgroundColor(colorNum[answerArray[0][0]]);
        aBR.setBackgroundColor(colorNum[answerArray[0][1]]);
        aTL.setBackgroundColor(colorNum[answerArray[0][2]]);
        aBL.setBackgroundColor(colorNum[answerArray[0][3]]);

        bTR.setBackgroundColor(colorNum[answerArray[1][0]]);
        bBR.setBackgroundColor(colorNum[answerArray[1][1]]);
        bTL.setBackgroundColor(colorNum[answerArray[1][2]]);
        bBL.setBackgroundColor(colorNum[answerArray[1][3]]);

        cTR.setBackgroundColor(colorNum[answerArray[2][0]]);
        cBR.setBackgroundColor(colorNum[answerArray[2][1]]);
        cTL.setBackgroundColor(colorNum[answerArray[2][2]]);
        cBL.setBackgroundColor(colorNum[answerArray[2][3]]);

        dTR.setBackgroundColor(colorNum[answerArray[3][0]]);
        dBR.setBackgroundColor(colorNum[answerArray[3][1]]);
        dTL.setBackgroundColor(colorNum[answerArray[3][2]]);
        dBL.setBackgroundColor(colorNum[answerArray[3][3]]);

    }

    public static String formatScore(long score) {
        DecimalFormat nf3 = new DecimalFormat("000");


        return     nf3.format(score);

    }

    public static String formatTimer(int timer){
        DecimalFormat nf3 = new DecimalFormat("00");


        return     nf3.format(timer);

    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    private void loadScoreOfLeaderBoard() {
        Games.Leaderboards.loadCurrentPlayerLeaderboardScore(getApiClient(), getString(R.string.leaderboard_finding_the_chosen_one), LeaderboardVariant.TIME_SPAN_ALL_TIME, LeaderboardVariant.COLLECTION_PUBLIC).setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
            @Override
            public void onResult(final Leaderboards.LoadPlayerScoreResult scoreResult) {
                if (isScoreResultValid(scoreResult)) {
                    // here you can get the score like this
                     mPoints = scoreResult.getScore().getRawScore();
                    String s = String.valueOf(mPoints);
                    Log.i("HighScore",s);

                }
            }
        });
    }
    private boolean isScoreResultValid(final Leaderboards.LoadPlayerScoreResult scoreResult) {
        return scoreResult != null && GamesStatusCodes.STATUS_OK == scoreResult.getStatus().getStatusCode() && scoreResult.getScore() != null;
    }


    private class MyCountDown extends CountDownTimer
    {
        public MyCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            start();
        }

        @Override
        public void onFinish() {
            // I have an Intent you might not need one
            timer.setText("0:00");
            if(countdown == 0){
                MediaPlayer mp = MediaPlayer.create(GameActivity.this, R.raw.wrong);
                mp.start();
                if(isSignedIn()) {
                    Games.Leaderboards.submitScore(getApiClient(),
                            getString(R.string.leaderboard_finding_the_chosen_one), scoreCounter);
                }
                String s = String.valueOf(mPoints);

                gameOverIntent.putExtra("highScore",s);
                gameOverIntent.putExtra("score",score.getText());
                startActivity(gameOverIntent);
                GameActivity.this.finish();
            }else{
                t.start();
            }
        }


        @Override
        public void onTick(long duration) {
            timer.setText(String.format("0:%02d", (int) duration / 1000));
            scoreAdder = (int)duration/1000;
            if (countdown > 0) {
                countdown -= 1;
            }
        }
        }
    }
